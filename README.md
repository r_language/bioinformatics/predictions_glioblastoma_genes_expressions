In this R file we created multiple classifiers : 

- Multiclasspairs

- Random Forest

- Pamr

- Random Forest SRC 

We used those classifiers to perform predictions on test data, find top k genes (the most differentially expressed ones) based on glioblastoma (genes expressions) data. We also compared the classifiers in the end (accuracy, kappa, etc). You have to change setwd() at the beginning to adjust with your own path. And if you change data, you might have a few commands to adapt (number of classes, etc). You can also see the HTML output. 

Author : Marion Estoup 

Mail : marion_110@hotmail.fr

Date : August 2023

